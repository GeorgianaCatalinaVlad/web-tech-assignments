
const FIRST_NAME = "Vlad";
const LAST_NAME = "Georgiana-Catalina";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
function numberParser(g) {
   
    if(g>Number.MAX_SAFE_INTEGER || g<Number.MIN_SAFE_INTEGER)
    {g=NaN;}
  return Number.parseInt(g);
  
}



module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

